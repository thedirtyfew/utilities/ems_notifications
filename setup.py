from setuptools import setup
import pathlib
import os

HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()


setup(
    name='ems-notifications',
    author="Jesper Halkjær Jensen",
    author_email="jsjes@vestas.com",
    description="A utility package to streamline notifications",
    version=os.getenv("CI_COMMIT_TAG", "v0.0.1").strip("v"),
    url='https://gitlab.com/thedirtyfew/utilities/ems_notifications/',
    packages=['ems_notifications', 'scripts'],
    package_data={
        'ems_notifications': ['res/*.html']
    },
    entry_points={
        'console_scripts': [
            'test-notify=scripts.test_notify:run'
        ]
    },
    long_description=README,
    long_description_content_type="text/markdown",
    license="MIT",
    python_requires='>=3.6',
    install_requires=['pymsteams', 'jinja2']
)
