# EMS Notifications

## Installation

```
pip install --extra-index-url https://gitlab.com/api/v4/projects/20076287/packages/pypi/simple ems-notifications
```
You can use the following environment variables:

## Email
```
SMTP_SERVER
SMTP_PORT
SENDER_EMAIL

SMTP_USERNAME # Optional - can be ignored if no login is required
SMTP_PASSWORD # Optional - can be ignored if no login is required
```

## Teams
```
TEAMS_WEBHOOK_URL
```