import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging

from jinja2 import Template
import pkgutil

from ems_notifications.notifier import BaseNotifier

_SMTP_URL = os.getenv("SMTP_SERVER", "internal-Relay.vestas.net")
_SMTP_PORT = int(os.getenv("SMTP_PORT", 25))
_SMTP_USERNAME = os.getenv("SMTP_USERNAME", None)
_SMTP_PASSWORD = os.getenv("SMTP_PASSWORD", None)
_SENDER_EMAIL = os.getenv("SENDER_EMAIL", "noreply@ems.com")

template_data = pkgutil.get_data(__name__, "res/template.html").decode("utf8")
t = Template(template_data)

_logger = logging.getLogger("ems_notifications (email)")
_logger.info(f"{_SMTP_USERNAME}@{_SMTP_URL}:{_SMTP_PORT}")
_logger.info(f"Sender address: {_SENDER_EMAIL}")


def _generate_email(receiver, title, main_text=None, sections=None, link=None):
    message = MIMEMultipart("alternative")
    message["Subject"] = title
    message["From"] = _SENDER_EMAIL
    message["To"] = receiver

    if sections:
        sections = [(s[0], s[1].replace("\n", "<br>")) for s in sections]
    else:
        sections = []

    html = t.render(title=title, main_text=main_text.replace("\n", "<br>"), sections=sections, link=link)

    message.attach(MIMEText(html, "html"))

    _logger.debug("Sending to " + receiver)

    with smtplib.SMTP(_SMTP_URL, _SMTP_PORT) as server:

        if _SMTP_USERNAME:
            server.login(_SMTP_USERNAME, _SMTP_PASSWORD)
        k = server.sendmail(
            _SENDER_EMAIL, receiver, message.as_string()
        )
        return k


class EmailNotifier(BaseNotifier):

    def __init__(self, email_listeners=None, as_mass_email=False):
        self._email_listeners = {}
        if email_listeners:
            for email in email_listeners:
                self._email_listeners[email] = None
        self._mass_email = as_mass_email

    def with_emails(self, *args):
        for email in args:
            self._email_listeners[email] = None
        return self

    def as_mass_email(self):
        self._mass_email = True
        return self

    def get_registered_emails(self):
        return self._email_listeners.keys()

    def send_notification(self, title, main_text=None, sections=None, link=None):
        if self._mass_email:
            listeners = [", ".join(self.get_registered_emails())]
        else:
            listeners = self.get_registered_emails()

        for email in listeners:
            _generate_email(email, title, main_text=main_text, sections=sections, link=link)

