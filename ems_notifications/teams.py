import pymsteams
import os
import logging

from ems_notifications import BaseNotifier

TEAMS_WEBHOOK_URL = os.getenv("TEAMS_WEBHOOK_URL", None)

_logger = logging.getLogger("ems_notifications (teams)")
if TEAMS_WEBHOOK_URL is None:
    _logger.info("No Teams Webhook has been configured...")


class TeamsNotifier(BaseNotifier):

    def __init__(self, webhook=None):
        if webhook:
            self._web_hook_url = webhook
        else:
            self._web_hook_url = TEAMS_WEBHOOK_URL

    def web_hook(self, webhook):
        self._web_hook_url = webhook
        return self

    def send_notification(self, title, main_text=None, sections=None, link=None):

        msg = pymsteams.connectorcard(self._web_hook_url)

        msg.title(title)
        msg.text(main_text.replace("\n", "   \n"))
        if sections:
            for s_title, s_text in sections:
                sec = pymsteams.cardsection()
                sec.title(s_title)
                sec.text(s_text.replace("\n", "   \n"))

                msg.addSection(sec)

        if link:
            if not len(link) == 2:
                raise ValueError("Link should be a tuple of length 2 with title and href")
            msg.addLinkButton(link[0], link[1])
        return msg.send()
