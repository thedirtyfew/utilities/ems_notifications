import functools
from concurrent.futures.thread import ThreadPoolExecutor

_executor = ThreadPoolExecutor(max_workers=10)


def wait_for_async():
    _executor.shutdown(wait=True)


class BaseNotifier:

    def send_notification(self, title, main_text=None, sections=None, link=None):
        raise NotImplementedError

    def send_notification_async(self, title, main_text=None, sections=None, link=None):

        f = functools.partial(self.send_notification, title, main_text=main_text, sections=sections, link=link)
        _executor.submit(f)