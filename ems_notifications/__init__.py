from ems_notifications.email import *
from ems_notifications.teams import *


class CombiNotifier(BaseNotifier):

    def __init__(self):
        self._notifiers = []

    def add_notifier(self, notifier):
        self._notifiers.append(notifier)
        return self

    def send_notification(self, title, main_text=None, sections=None, link=None):
        for n in self._notifiers:
            n.send_notification(title, main_text=main_text, sections=sections, link=link)