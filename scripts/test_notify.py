import logging
from argparse import ArgumentParser
from ems_notifications import TeamsNotifier, EmailNotifier, CombiNotifier, TEAMS_WEBHOOK_URL
from ems_notifications.notifier import wait_for_async


def run():
    parser = ArgumentParser()
    parser.add_argument("--email", required=False)
    parser.add_argument("--bg", action="store_true", required=False)

    args = parser.parse_args()

    c = CombiNotifier()

    if args.email:
        c.add_notifier(EmailNotifier().with_emails(args.email))
    else:
        logging.warning("No emails registered")

    if TEAMS_WEBHOOK_URL:
        c.add_notifier(TeamsNotifier())

    if args.bg:
        f = c.send_notification_async
    else:
        f = c.send_notification

    f("Title of email",
      main_text="This is the main text",
      sections=[
        ("Section 1", "This is section 1 text"),
        ("Section 2", "This is section 2 text")
      ],
      link=("This is a link to google.com", "https://google.com"))

    if args.bg:
        wait_for_async()


if __name__ == "__main__":
    run()
